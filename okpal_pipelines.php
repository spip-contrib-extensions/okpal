<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function okpal_affichage_final($html) {
	// S'il y a au moins un bouton Okpal trouvé quelque part
	if (strpos($html, 'okpal-checkout-button') !== false) {
		include_spip('inc/config');
		
		// On scanne chacun des boutons trouvés
		if (preg_match_all('|"okpal-checkout-button-(\w+)"( data-lang="(\w+)")?|is', $html, $boutons, PREG_SET_ORDER)) {
			$ids_a_inserer = array();
			$langs_a_inserer = array();
			
			foreach ($boutons as $bouton) {
				if (!in_array($bouton[1], $ids_a_inserer)) {
					$ids_a_inserer[] = $bouton[1];
					$langs_a_inserer[] = $bouton[3];
				}
			}
			
			// Pour chaque campagne unique, on insère le script
			$scripts = '';
			foreach ($ids_a_inserer as $i => $id) {
				$scripts .= '<script async src="https://www.okpal.com/embed/checkout" data-id="okpal" data-project-id="'.$id.'" data-container="okpal-checkout-button-'.$id.'"';
				if ($langs_a_inserer[$i]) {
					$scripts .= ' data-lang="'.$langs_a_inserer[$i].'"';
				}
				$scripts .= '></script>';
				
				// Si le label doit être changé
				if ($bouton_label = lire_config('okpal/bouton_label')) {
					$scripts .= "<script>window.addEventListener('okpal:ready', function() {var btn = document.getElementById('okpal-button'); btn.innerHTML = '$bouton_label';});</script>";
				}
			}
			
			$html = str_replace('</body>', "$scripts</body>", $html);
		}
	}
	
	return $html;
}
